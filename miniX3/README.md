# Throbber MiniX3

My code represents a form of "throbber," a concept within digital culture typically used to signal an ongoing process while the user waits. It can help manage user expectations by signaling that something is happening in the background while they wait. This can reduce frustration during the wait and create a more pleasant user experience. However, throbbers also hide information about the progress of the process. They don't provide specific details about how long the process will take or what steps are being performed. This lack of transparency can sometimes lead to user frustration or uncertainty.

Overall, my code combines interactivity and visual feedback to create an experience that is both functional and engaging. By using the throbber concept in a new and creative way, my code allows users to participate in the process while signaling that something is happening behind the scenes.

![Billede af min MiniX1](MiniX3.png "Throbber")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/miniX3/)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[Lines](https://p5js.org/examples/drawing-continuous-lines.html)
[Throbber](https://editor.p5js.org/fleshcircuit/sketches/CJPmAiJzB)