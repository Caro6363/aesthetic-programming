# Final Project: Life Prediction 
Link til ReadME: 
https://gitlab.com/Caro6363/aesthetic-programming/-/blob/main/FinalProject/ReadMe.pdf 

![Billede af vores program](FinalProject.png "Final Project")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/FinalProject/)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>

[Text generator inspi](https://gitlab.com/apinstruktor20241/ap-codebase/-/blob/main/p5js/ai_chatbot_examples/openai_basic/openAIExample.js?ref_type=heads)

[Image generator inspi](https://ap-codebase-apinstruktor20241-833dc1b643c2e37739696091f14798e65.gitlab.io/p5js/ai_chatbot_examples/dall-e_example/?fbclid=IwZXh0bgNhZW0CMTAAAR3lFVyzmVqJ5sKo0DrB3_YRQQI40o4yBkWhs3iGolCC7L4a-eVrP98kVzo_aem_AbaCCPVPTv7ipdbklXFOhMDKYQ4gxKbzFlwOm4ztMFsCwir5kmNSeev3jpIrxFHQQbS-oJl6VTuw-sYrZK96sRiF)

[OpenAI](https://platform.openai.com/docs/guides/text-generation/faq)

[ChatGPT4](https://platform.openai.com/docs/models/gpt-4o)

[Platform images](https://platform.openai.com/docs/guides/images)

[Prompt](https://platform.openai.com/docs/guides/prompt-engineering)
