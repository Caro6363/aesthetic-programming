let api = 'https://api.giphy.com/v1/gifs/search?';
let apiKey = '&api_key=ySNwU0gntaB53DiRDqOmQeSeil3qNU0S';
let query = '';


function setup() {
  noCanvas();
}

function searchGiphy() {
  let input = document.getElementById('searchInput').value;
  query = '&q=' + input;
  let url = api + apiKey + query;
  loadJSON(url, gotData);
}

function gotData(giphy) {
  let gifsContainer = document.getElementById('gifsContainer');
  gifsContainer.innerHTML = ''; // Clear previous search results
  for (let i = 0; i < giphy.data.length; i++) {


      let img = createImg(giphy.data[i].images.original.url);
      img.parent(gifsContainer);
    }
  }