# Working with APIs - MiniX8

The program is a simple web application that allows users to search for GIFs using the Giphy API. Users can enter a search term, click a button to perform the search, and the application will display GIFs related to the search term. The Giphy API was chosen because it provides a big collection of GIFs and offers an easy-to-use interface for searching and retrieving GIF data.

Acquiring: The program acquires GIF data from the Giphy API by sending HTTP requests with specific parameters, such as the search query.
Processing: The received data (in JSON format) is processed by the program to extract relevant information, such as the URLs of the GIFs, which are then displayed on the web page.
Using: The processed data is used to dynamically update the web page with the retrieved GIFs.
Representing: The GIFs are represented visually on the web page for the user to interact with.

While the program demonstrates a basic understanding of acquiring and using data from a web API, there's always more to learn, especially regarding how platform providers sort and filter data. Power relations in APIs can involve issues such as data ownership, access restrictions, and the influence of platform policies on what data can be accessed and how it can be used. APIs play a significant role in digital culture by enabling developers to access and integrate various services and datasets, facilitating innovation and the creation of new digital experiences.

<b>Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.<b>

<p>How do platform providers ensure the accuracy and relevance of the data returned through their APIs, especially when dealing with user-generated content such as GIFs? Are there mechanisms in place to filter out inappropriate or low-quality content?<p>

![Billede af min MiniX8](Home.png "MiniX8")
![Billede af min MiniX8](GIF.png "MiniX8")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/MiniX8/)]

<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[p5 Reference](https://p5js.org/reference/#group-Shape)
[createImg()](https://p5js.org/reference/#/p5/createImgt)
[noCanvas()](https://p5js.org/reference/#/p5/noCanvas)
[loadJSON()](https://p5js.org/reference/#/p5/loadJSON)
[setup()](https://p5js.org/reference/#/p5/setup)
[createCanvas()](https://p5js.org/reference/#/p5/createCanvas)
