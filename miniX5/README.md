# Doodles MiniX5
The code utilizes the p5.js library to create a canvas and draw lines on it. It defines variables for the x and y positions of the lines, as well as the spacing between them. In the draw() function, lines are drawn diagonally based on random conditions. Randomness is introduced in line colors, thicknesses, and lengths. The y-position of the lines is affected by Perlin noise, adding variation over time. The program loops through the canvas, continuously drawing lines until the y-position exceeds the canvas height, at which point drawing stops.

The rules in the generative program determine how the lines are drawn on the canvas. Specifically:

1. Lines are drawn diagonally from either top-left to bottom-right or top-right to bottom-left.
2. Colors, thickness, and lengths of the lines are randomly determined.
3. The y-position of the lines is influenced by Perlin noise, introducing a level of randomness and variation.

As the program runs, it continuously generates and draws lines according to the specified rules. The lines accumulate over time, creating a dynamic and evolving pattern on the canvas. 

![Billede af min MiniX5](MiniX5.png "Doodles")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/miniX5/)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[noise](https://p5js.org/reference/#/p5/noise)
[random](https://p5js.org/reference/#/p5/random)
[noLoop](https://p5js.org/reference/#/p5/noLoop)
