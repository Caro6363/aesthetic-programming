let x = 0;
let y = 0;
let spacing = 50;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255, 192, 203);
}

function draw() {
  // Add some randomness to colors
  let r = random(255);
  let g = random(255);
  let b = random(255);
  stroke(r, g, b);

  // Add randomness to line thickness
  let weight = random(1, 5);
  strokeWeight(weight);

  // Add randomness to line length
  let len = random(spacing / 2, spacing * 2);

  if (random(1) < 0.5) {
    // Draw a diagonal line from top-left to bottom-right
    line(x, y, x + len, y + len);
  } else {
    // Draw a diagonal line from top-right to bottom-left
    line(x, y + len, x + len, y);
  }

  x += spacing;

  // Add variation in y-position based on noise
  y += noise(x / 100) * 50 - 25;

  if (x > width) {
    x = 0;
    y += spacing;
  }

  // Add a boundary for y-position
  if (y > height) {
    noLoop(); // Stop drawing when y-position exceeds window height
  }
}
