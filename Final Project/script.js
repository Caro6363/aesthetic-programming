let currentQuestion = 1;
let totalQuestions = 8;
let questions = [
  "How old are you today?",
  "What's most important to you?",
  "What's your biggest fear?",
  "What does your dream vacation look like?",
  "What does your love-life look like currently?",
  "What does your dream house look like?",
  "What do you do in your spare time?",
  "What do you want to achieve before you die?"
]; // Array to store questions

let answers = []; // Array to store selected answers and corresponding questions

function startQuestions() {
  document.getElementById('front-page').style.display = 'none';
  document.getElementById('video-background').style.display = 'none'; // Hide the video
  document.body.style.backgroundColor = 'white'; // Change background color to white
  document.body.style.color = 'black'; // Change text color to black
  document.getElementById('form-container').style.display = 'flex';
  document.getElementById('question1').style.display = 'block';
}

function markAnswer(element) {
  // Remove 'selected' class from all input-box elements in current question
  let inputs = document.getElementById('question' + currentQuestion).getElementsByClassName('input-box');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].classList.remove('selected');
  }
  // Mark the clicked answer
  element.classList.add('selected');
  // Show the submit button
  document.getElementById('submit-btn').style.display = 'block';
}

function nextQuestion() {
  // Get selected answer
  let selectedAnswer = document.querySelector('#question' + currentQuestion + ' .selected');
  if (selectedAnswer) {
    // Store selected answer along with the corresponding question
    answers.push({ question: questions[currentQuestion - 1], answer: selectedAnswer.value });
    // Hide the current question
    document.getElementById('question' + currentQuestion).style.display = 'none';
    // Move to the next question
    currentQuestion++;
    if (currentQuestion <= totalQuestions) {
      // Display the next question
      document.getElementById('question' + currentQuestion).style.display = 'block';
      // Hide the submit button until an answer is selected
      document.getElementById('submit-btn').style.display = 'none';
    } else {
      // All questions answered, hide the next question button
      document.getElementById('next-question-btn').style.display = 'none';
      // Show the submit button for final action
      document.getElementById('submit-btn').style.display = 'block';
      document.getElementById('submit-btn').innerText = 'Generate Prediction';
    }
  } else {
    alert("Please select an answer before proceeding.");
  }
}

function submitAnswer() {
  // Get selected answer
  let selectedAnswer = document.querySelector('#question' + currentQuestion + ' .selected');
  if (selectedAnswer) {
    // Store the answer
    answers.push({ question: questions[currentQuestion - 1], answer: selectedAnswer.value });
    // Hide the current question
    document.getElementById('question' + currentQuestion).style.display = 'none';

    // Check if it's the last question
    if (currentQuestion === totalQuestions) {
      // Hide the form container after the last question
      document.getElementById('form-container').style.display = 'none';
      // Show the response container for displaying the result
      document.getElementById('response-container').style.display = 'block';
      // Generate the prediction and image
      generatePredictionAndImage();
    } else {
      // Move to the next question
      currentQuestion++;
      document.getElementById('question' + currentQuestion).style.display = 'block';
      document.getElementById('submit-btn').style.display = 'none';
    }
  } else {
    alert("Please select an answer before proceeding.");
  }
}

function generatePredictionAndImage() {
  console.log("Generating prediction and image...");

  // Constructing the user's responses based on the questions and answers array
  let userResponses = "";
  for (let i = 0; i < answers.length; i++) {
    userResponses += questions[i] + ": " + answers[i].answer + "\n";
  }
  console.log("User responses:\n", userResponses);

  // Constructing the request data with the user's responses for text generation
  let requestData = {
    "model": "gpt-3.5-turbo",
    "messages": [
      {
        "role": "system",
        "content": "You are a slightly mean fortune-teller. Describe a life prediction in 10 years depending on the answers"
      },
      {
        "role": "user",
        "content": userResponses
      }
    ]
  };

  console.log("Request Data for text generation:", requestData);

  // Sending the request to the API for text generation
  fetch('https://api.openai.com/v1/chat/completions', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer sk-proj-cdZkLTamva1Fbz6w3tFbT3BlbkFJvt2t7X0PfU4PLGZH3G6t' // Replace with your actual API key
    },
    body: JSON.stringify(requestData)
  })
    .then(response => response.json())
    .then(data => {
      console.log("Chatbot response data:", data);
      if (data.choices && data.choices.length > 0) {
        // Displaying the chatbot response
        const textResponse = data.choices[0].message.content;
        document.getElementById('response').innerText = textResponse;

        // Construct the image prompt using the answers directly
        let imagePrompt = "Create a realistic profile picture of a person with the following attributes:\n";
        for (let i = 0; i < answers.length; i++) {
          imagePrompt += questions[i] + ": " + answers[i].answer + "\n";
        }

        // Generate the image using the same description
        generateImage(imagePrompt); // Call the function to generate an image
      } else {
        console.error('No valid response from chatbot');
      }
    })
    .catch(error => {
      console.error('Error generating text:', error);
    });
}

function generateImage(description) {
  console.log("Generating image with description:", description);

  let imageRequestData = {
    "prompt": description,
    "n": 1,
    "size": "1024x1024"
  };

  console.log("Image Request Data:", imageRequestData);

  fetch('https://api.openai.com/v1/images/generations', { // Replace with the actual API endpoint for image generation
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer sk-proj-cdZkLTamva1Fbz6w3tFbT3BlbkFJvt2t7X0PfU4PLGZH3G6t' // Replace with your actual API key
    },
    body: JSON.stringify(imageRequestData)
  })
    .then(response => response.json())
    .then(data => {
      console.log("Image generation response data:", data);

      if (data && data.data && data.data.length > 0) {
        let imageUrl = data.data[0].url;
        let imageContainer = document.getElementById('image-container');
        let imgElement = document.createElement('img');
        imgElement.src = imageUrl;
        imgElement.id = 'generated-image';
        imageContainer.innerHTML = ''; // Clear any previous image
        imageContainer.appendChild(imgElement);
      } else {
        console.error('No image URL found in response:', data);
      }
    })
    .catch(error => {
      console.error('Error generating image:', error);
    });
}
