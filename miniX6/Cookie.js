class Cookie {
  constructor() {
    this.speed = cookieSpeed; // Speed based on current level
    this.pos = createVector(width + 5, random(12, height / 1.7));
    this.size = floor(random(30, 50));
    this.cookie_rotate = random(0, PI / 20);
    this.emoji_size = this.size / 1.8;
    this.value = floor(random(1, 5)); // Random value assigned to the cookie
  }
  move() {
    this.pos.x -= this.speed;
  }
  show() {
    push();
    translate(this.pos.x, this.pos.y);
    rotate(this.cookie_rotate);
    noStroke();
    fill(80); // Cookie texture
    textStyle(BOLD);
    textSize(this.emoji_size);
    text('🍪', 0, 0); // Cookie symbol
    pop();
  }
}

class PowerUp {
  constructor() {
    this.speed = 2;
    this.pos = createVector(width + 5, random(12, height / 1.7));
    this.size = 40;
    this.speedIncrease = 0.5; // Speed increase for power-up
    this.symbol = '⚡'; // Symbol for speed power-up
  }
  move() {
    this.pos.x -= this.speed;
  }
  show() {
    push();
    translate(this.pos.x, this.pos.y);
    noStroke();
    fill(255, 165, 0); // Power-up color
    ellipse(0, 0, this.size, this.size); // Power-up symbol background
    fill(255); // Symbol color
    textSize(24);
    textAlign(CENTER, CENTER);
    text(this.symbol, 0, 0); // Power-up symbol
    pop();
  }

  applySpeedIncrease() {
    // Function to apply speed increase on cookies
    for (let i = 0; i < cookies.length; i++) {
      cookies[i].speed += this.speedIncrease;
    }
  }
}