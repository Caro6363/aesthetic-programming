let pacmanSize = {
  w: 86,
  h: 89
};
let pacman;
let pacPosY;
let mini_height;
let min_cookies = 5; // Minimum number of cookies on the screen
let cookies = [];
let powerUps = [];
let score = 0,
  lose = 0;
let keyColor = 45;
let level = 1;
let cookieSpeed = 3;

function preload() {
  pacman = loadImage("pacman.gif");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  pacPosY = height / 2;
  mini_height = height / 2;
}

function draw() {
  background(173, 216, 230, 100);
  fill(keyColor, 255);
  rect(0, height / 1.5, width, 1);
  displayScore();
  checkCookiesNum(); // Check available cookies
  showCookies();
  showPowerUps(); // Display power-ups
  image(pacman, 0, pacPosY, pacmanSize.w, pacmanSize.h);
  checkEating(); // Check if Pacman eats cookies or power-ups
  checkResult();
}

function checkCookiesNum() {
  if (cookies.length < min_cookies) {
    cookies.push(new Cookie());
  }
  if (random(1) < 0.01) { // Chance of spawning power-ups
    powerUps.push(new PowerUp());
  }
}

function showCookies() {
  for (let i = 0; i < cookies.length; i++) {
    cookies[i].move();
    cookies[i].show();
  }
}

function showPowerUps() {
  for (let i = 0; i < powerUps.length; i++) {
    powerUps[i].move();
    powerUps[i].show();
  }
}

function checkEating() {
  for (let i = 0; i < cookies.length; i++) {
    let d = int(
      dist(
        pacmanSize.w / 2,
        pacPosY + pacmanSize.h / 2,
        cookies[i].pos.x,
        cookies[i].pos.y
      )
    );
    if (d < pacmanSize.w / 2.5) {
      score += cookies[i].value; // Increment score by cookie's value
      cookies.splice(i, 1);
      levelUp(); // Check if the player leveled up
    } else if (cookies[i].pos.x < 3) {
      lose++;
      cookies.splice(i, 1);
    }
  }
  
  for (let i = 0; i < powerUps.length; i++) {
    let d = int(
      dist(
        pacmanSize.w / 2,
        pacPosY + pacmanSize.h / 2,
        powerUps[i].pos.x,
        powerUps[i].pos.y
      )
    );
    if (d < pacmanSize.w / 2.5) {
      powerUps[i].applySpeedIncrease(); // Apply speed increase
      powerUps.splice(i, 1);
    }
  }
}

function displayScore() {
  fill(keyColor, 160);
  textSize(17);
  text('Score: ' + score, 10, height / 1.4); // Display current score
  text('Cookies wasted: ' + lose, 10, height / 1.4 + 20); // Display cookies wasted
  fill(keyColor, 255);
  text(
    'PRESS the ARROW UP & DOWN key to move Pacman',
    10,
    height / 1.4 + 40
  );
  text('Level: ' + level, 10, height / 1.4 + 60); // Display current level
}

function checkResult() {
  if (lose > score && lose > 2) {
    fill(keyColor, 255);
    textSize(26);
    text("Too much wastage... GAME OVER", width / 3, height / 1.4);
    noLoop();
  }
}

function levelUp() {
  if (score >= level * 10) { // Increment level every 10 points
    level++;
    cookieSpeed += 0.5; // Increase cookie speed
  }
}

function keyPressed() {
  if (keyCode === UP_ARROW) {
    pacPosY -= 50;
  } else if (keyCode === DOWN_ARROW) {
    pacPosY += 50;
  }
  // Reset if the pacman moves out of range
  if (pacPosY > mini_height) {
    pacPosY = mini_height;
  } else if (pacPosY < 0 - pacmanSize.w / 2) {
    pacPosY = 0;
  }
}