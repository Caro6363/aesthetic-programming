# PACMAN - MiniX6

In my game, the player controls Pacman using the arrow keys to navigate through the screen. The goal is to eat as many cookies as possible while avoiding waste. Cookies and power-ups randomly spawn on the screen, and Pacman must eat them to gain points or trigger certain effects. The game objects include Pacman, cookies, and power-ups. Pacman's position is controlled by the player's input, while cookies and power-ups move horizontally across the screen. When Pacman comes into contact with a cookie, the player's score increases based on the cookie's value. Power-ups increases the speed of cookies.

Each game object (Pacman, cookies, and power-ups) is represented by a class with its own set of attributes and methods. For example, the Cookie class has attributes such as speed, position, size, and value, and methods to move and display the cookie on the screen. Similarly, the PowerUp class has attributes for speed, position, size, and the type of power-up, along with methods to move and display the power-up symbol. The main game loop checks for collisions between Pacman and cookies/power-ups, updates the score accordingly, and applies any effects from power-ups.

![Billede af min MiniX1](MiniX6.png "MiniX6")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/miniX6/)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[p5 Reference](https://p5js.org/reference/#group-Shape)
[dist](https://p5js.org/reference/#/p5/dist)
[setup()](https://p5js.org/reference/#/p5/setup)
[show](https://p5js.org/reference/#/p5.Element/show)
