# Flowchart - MiniX9

<b>What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?<b>

The difficulties lie in balancing these two aspects. Simplifying communication while maintaining algorithmic complexity requires careful abstraction and summarization. Communicating complex algorithms in a simple way without losing essential details can be challenging.

<b> What are the technical challenges facing the two ideas and how are you going to address these?<b>

Simplifying communication: When we try to make things easy to understand, we need to avoid using complicated words, use simple pictures like flowcharts, and explain things clearly. To do this well, we need to know how much our audience knows about the topic and adjust what we say accordingly. We also need to improve our language and pictures by listening to feedback and making changes.

Maintaining algorithmic complexity: Making sure our complicated procedures work well involves challenges like making the steps efficient, dealing with unusual situations, and making things run smoothly. To tackle these challenges, we need to test our work, have others check our code, and maybe rearrange things to make them clearer and more efficient without making them less complicated.

![Billede af min flowchart](Flowchart.png "Flowchart")

<br>

<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[p5 Reference](https://p5js.org/reference/#group-Shape)

