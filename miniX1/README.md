# Penguin MiniX1

In this code, I have created a simple animation of falling snowflakes on a blue background. The sketch consists of a snowflake class that defines the behavior and appearance of each snowflake, and a setup and draw function that handle the creation and display of the snowflakes.

In the setup function, I created a canvas with a size of 500x500 pixels using the createCanvas function. Additionally, I have drawn a simple snowman figure using basic shapes such as circles and triangles. The snowman consists of a body, a head, eyes, a nose, and arms. Each shape is drawn with specific coordinates and colors using the fill function. Overall, creating this animation was an enjoyable experience. It allowed me to explore p5.js and practice my programming.

![Billede af min MiniX1](Penguin.png "Penguin")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/miniX1/)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[Snowflakes](https://p5js.org/examples/simulate-snowflakes.html)
