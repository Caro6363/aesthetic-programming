let snowflakes = []; // array to hold snowflake objects

function setup() {
 createCanvas(500, 500);
}

function draw() {
background (0,0,255)
let t = frameCount / 60; // update time

// create a random number of snowflakes each frame
for (let i = 0; i < random(5); i++) {
  snowflakes.push(new snowflake()); // append snowflake object
}

// loop through snowflakes with a for..of loop
for (let flake of snowflakes) {
  flake.update(t); // update snowflake position
  flake.display(); // draw snowflake
}
}

// snowflake class
function snowflake() {
// initialize coordinates
this.posX = 0;
this.posY = random(-50, 0);
this.initialangle = random(0, 2 * PI);
this.size = random(2, 5);

// radius of snowflake spiral
// chosen so the snowflakes are uniformly spread out in area
this.radius = sqrt(random(pow(width / 2, 2)));

this.update = function(time) {
  // x position follows a circle
  let w = 0.6; // angular speed
  let angle = w * time + this.initialangle;
  this.posX = width / 2 + this.radius * sin(angle);

  // different size snowflakes fall at slightly different y speeds
  this.posY += pow(this.size, 0.5);

  // delete snowflake if past end of screen
  if (this.posY > height) {
    let index = snowflakes.indexOf(this);
    snowflakes.splice(index, 1);
  }
};

this.display = function() {
  fill(255); // Set fill color to white
  ellipse(this.posX, this.posY, this.size);
};

fill(0); // Color black
ellipse(250, 350, 100, 150); //Body 

fill(255,255,255); // Color white
ellipse(250, 350, 75, 100); // Body inside

fill(0); // Color Black
circle(250,250,100); // Head 

fill(255,255,255); // Color white
circle(250,250,80); // Head inside

fill(0); // Color Black
circle(230,240,10); // Eye left

fill(0); //Color Black
circle(270,240,10); // Eye Right

fill(255, 165, 0); // Color Orange
noStroke () 
triangle(270, 260, 230, 260, 250, 280) // Nose

fill(255, 165, 0); // Color Orange
noStroke () 
triangle(200, 425, 215, 410, 230, 425) // Right Feet

fill(255, 165, 0); // Color Orange
noStroke () 
triangle(270, 425, 285, 410, 300, 425)// Left Feet

fill(0); // Color Black
noStroke () 
triangle(280, 310, 280, 285, 350, 350); //R Arm

fill(0); // Color Black
noStroke () 
triangle(220, 310, 220, 285, 150, 350); //L Arm

}


