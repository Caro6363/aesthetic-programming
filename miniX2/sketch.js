let rSlider;
let gSlider;

function setup() {
 // put setup code here
 createCanvas(1000, 500);

 rSlider = createSlider(0, 255, 255); // Range from 0 to 255, starting at (255, 255, 0) creating a yellow smiley
 rSlider.position(20, 20); // Position of sliders

 gSlider = createSlider(0, 255, 255);
 gSlider.position(20, 50);
}

function draw() {
  // put drawing code here
  background (173, 216, 230);

  let r = rSlider.value();
  let g = gSlider.value();
    
  // Smiley V
  fill (r, 255, 0)
  noStroke (0)
  circle(250, 250, 300);

  // Øje V
  fill (0)
  ellipse(200, 180, 55, 30);

  fill (255,255,255)
  noStroke (0)
  circle(200, 180, 25);

    // Øje H
    fill (0)
    ellipse(300, 180, 55, 30);

    fill (255,255,255)
    noStroke (0)
    circle(300, 180, 25);

   // Mund 
   fill(0);
   noStroke();
   arc(250, 290, 200, 120, 0, PI); // Smiling mouth

   if (mouseX < 350) {
    push();
    noStroke();
    // blush
    fill(255, 126, 179, 127);
    ellipse(175, 250, 80, 40)
    ellipse(325, 250, 80, 40)
    pop();

  }

  // Smiley H
  fill (g, 162, 200)
  noStroke (0)
  circle(750, 250, 300);

    // Øje V2
    fill (0)
    ellipse(700, 180, 55, 30);

    fill (255,255,255)
    noStroke (0)
    circle(700, 180, 25);
  
      // Øje H2
      fill (0)
      ellipse(800, 180, 55, 30);

      fill (255,255,255)
      noStroke (0)
      circle(800, 180, 25);
  
      //Mund2
  fill(0);
  noStroke();
  arc(750, 325, 200, 120, PI, TWO_PI); // Sad mouth

  if (mouseX > 550) {
    push();
    noStroke();
    // tears
    fill(135, 206, 235, 127);
    rect(677.5, 185, 45, 400);
    rect(777.5, 185, 45, 400);
    pop();

  }
 
}
