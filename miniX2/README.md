# Emoji MiniX2

My program creates an interactive visual experience where users can control the color of two smileys using sliders. The smileys display different facial expressions based on mouse interaction, with one smiling and the other displaying a sad expression with tears.

The program utilizes p5.js functions such as createCanvas(), createSlider(), fill(), noStroke(), circle(), ellipse(), arc(), and background() to create and manipulate graphical elements on the canvas.

Emojis and visual representations play a significant role in how individuals and groups are portrayed and understood. The use of smileys in your program reflects a simplified representation of human emotions. However, it's essential to recognize that such representations can be limited and may not fully capture the complexity of human experiences and identities.

The choice of using smileys as the main visual elements raises questions about identity and race. Traditional yellow smileys have been criticized for lacking diversity and excluding racial minorities. By allowing users to change the color of the smileys, my program offers a degree of customization and potential for representation. 

![Billede af min MiniX2](MiniX2.png "Emoji")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/miniX2/)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[Sliders](https://p5js.org/reference/#/p5/createSlider)
[Let](https://p5js.org/reference/#/p5/let)
[MouseX](https://p5js.org/reference/#/p5/mouseX)
[MouseX](https://p5js.org/reference/#/p5/mouseX)