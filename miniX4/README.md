# Heartbeat Symphony - MiniX4

"Heartbeat Symphony" is an interactive audiovisual experience that transforms the user's voice into a vibrant display of color and motion. As users scream and shout into their microphones, their voices trigger changes in the background color and the size of a heart-shaped visual element. 

The program utilizes the p5.js library to create a canvas where users can interact with their microphone input. The audio input is captured using the p5.AudioIn object, and the volume level is continuously monitored. Based on the volume level, the background color changes from pink to blue, and the size of the heart element adjusts accordingly. The visual elements are drawn using basic geometric shapes and bezier curves.

"Heartbeat Symphony" encapsulates the theme of "capture all" by providing a platform for users to express themselves freely through sound. By capturing the user's voice input and translating it into visual and auditory feedback, the program captures the essence of the user's emotions and energy in real-time.

The cultural implications of data capture are multifaceted. On one hand, data capture allows for innovative and immersive experiences like "Heartbeat Symphony," enabling new forms of artistic expression and interaction. However, concerns about privacy and data security also arise, as personal voice data is collected and processed. It prompts reflection on the balance between technological advancement and individual autonomy in the digital age.

![Billede af min MiniX1](MiniX4.png "MiniX4")
![Billede af min MiniX1](MiniX42.png "MiniX4")

<br>

Please run the code [[here](https://Caro6363.gitlab.io/aesthetic-programming/miniX4/)]
<!--- Linket skal formatteres sådan som jeg har gjort det - https://{GRUPPE NAVN}.gitlab.io/{PROJEKT NAVN}{miniX DER SKAL SES}-->
<br>
Please view the full repository [https://gitlab.com/Caro6363/aesthetic-programming/-/tree/main?ref_type=heads]
<!--- Det her link kan I bare kopiere fra jeres browser, kopier linked når I befinder jer I mappen -->

### **References**
<!-- Eksempler på referencer her, pretty much bare links med line breaks mellem dem-->
<br>
[p5 Reference](https://p5js.org/reference/#group-Shape)
[p5.AudioIn](https://p5js.org/reference/#/p5.AudioIn)
[getLevel()](https://p5js.org/reference/#/p5.AudioIn/getLevel)