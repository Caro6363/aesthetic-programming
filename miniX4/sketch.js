let mic;
let pinkColor;
let blueColor;

function setup() {
  createCanvas(400, 450); // For at give plads til overskriften
  
  // Opret en lydindgangsobjekt
  mic = new p5.AudioIn();
  mic.start();

  // Definer farverne
  pinkColor = color(255, 102, 204); // Pink farve
  blueColor = color(51, 102, 255); // Blå farve
}

function draw() {
  // Få lydniveauet (mellem 0 og 1)
  let vol = mic.getLevel();
  
  // Interpolér farverne baseret på lydstyrken
  let bgColor = lerpColor(pinkColor, blueColor, vol);

  // Opdater baggrundsfarven
  background(bgColor);
  
  // Definer hjertets størrelse baseret på lydstyrken
  let heartSize = map(vol, 0, 1, 50, 200);
  
  // Placer hjertet i midten af vinduet
  let x = width / 2;
  let y = height / 2 - heartSize / 2;
  
  // Tegn hjertet
  drawHeart(x, y, heartSize);
  
  // Vis teksten med lydstyrke
  textAlign(CENTER, BOTTOM);
  textSize(20);
  fill(0);
  text("Volume: " + vol.toFixed(2), width/2, height - 40);

  // Vis overskriften
  textSize(24);
  text("Scream and shout and let it all out", width/2, 30);
}

// Funktion til at tegne et hjerte
function drawHeart(x, y, size) {
  beginShape();
  vertex(x, y + size / 4);
  bezierVertex(x + size / 2, y - size / 2, x + size, y + size / 4, x, y + size);
  bezierVertex(x - size, y + size / 4, x - size / 2, y - size / 2, x, y + size / 4);
  endShape(CLOSE);
}
